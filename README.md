# Release-Channel Metadata Generator
This tool can be used for generating metadata for Emulation-as-a-Service's release-channels.
It provides multiple commands to assist in creating correctly-formatted metadata.

# How to use
Currently, the generator is implemented as a single Python 3 script, using standard dependencies.
Hence it should be runnable as-is without any installation. To get an overview of all available
sub-commands and options, use the in-built documentation:
```
$ ./metadata-generator --help
$ ./metadata-generator <sub-command> --help
```

## Initializing new Release-Channel
An empty release-channel metadata can be created with:
```
$ ./metadata-generator -f channels/example.json initialize <channel-name>
```

## Adding Release-Artifacts
Once a release-channel metadata is initialized, multiple artifacts can be created with:
```
$ ./metadata-generator -f channels/example.json add-artifact -t <type> -l <location>
```

If all checks pass, then a generated artifact ID will be printed to stdout.


### Credentials for Private Artifacts

If artifacts require custom headers to be accessible, those headers can be put in a JSON file
with the following content:
```json
[
	{
		"url": "https://private.com/projects/ui/artifacts",
		"headers": {
			"access-token": "8vbVYwKHHjNlQXonDGSayA"
		}
	},
	...,
	{
		"url": "https://private.com/projects/server/artifacts",
		"headers": {
			"access-token": "jIXlAx6oGvbmMDJfXy89hw"
		}
	}
]
```

If artifact's location matches one of the URL-prefixes defined in this file, then the
corresponding headers will be added to any related HTTP request made by the generator.

Credentials file can be specified with `-c/--credentials` option, for example:
```
$ ./metadata-generator -f channels/example.json -c creds.json add-artifact -t <type> -l <location>
```


## Adding Releases
Once required release artifacts have been added, a new release can be created with:
```
$ ./metadata-generator -f channels/example.json add-release -v <version> -a <aid-1> [<aid-2>...]
```

## Checking Metadata Consistency
When all artifacts and releases have been added, a final consistency check should be performed with:
```
$ ./metadata-generator -f channels/example.json check
```

This will run multiple consistency and validity checks on the provided release-channel metadata.
